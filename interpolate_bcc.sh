#!/bin/bash

NO_OVERWRITE="False" #setting to "True" will prevent the existing files being overwritten [useful for filling holes]

script_dir="/home/ucfaccb/Documents/local_repos/experience_1pt5"
model="bcc-csm1-1" #name of the gcm
pp_maindir="/home/ucfaccb/DATA/cmip5_processed_files"
pp_dir=$pp_maindir/$model
underscore="_"
hist_rundir="/data/CMIP/cmip5/output1/BCC/bcc-csm1-1/historical/mon/atmos/Amon/r1i1p1/v1/ts/"

#create 1961-1990 climatology
cd $pp_dir
hist_files=`ls $hist_rundir/ts_Amon_$model*.nc`
climo_file=ts_Amon_$model.climo_1961-1990.nc
months="01 02 03 04 05 06 07 08 09 10 11 12"
end_dt='1990-12-31'
if [ ! -f $climo_file ] || [ ! $NO_OVERWRITE == "True" ]; then
    if [ -f $climo_file ]; then
	rm $climo_file
    fi
    for mon in $months
    do
	start_dt="1961-$mon-01"
	ncra -F -d time,$start_dt,$end_dt,12 $hist_files /tmp/$mon$climo_file
    done
fi
ncrcat /tmp/*$climo_file $climo_file
rm /tmp/*$climo_file

#Remove climatology from hist files
## Trying to do this in shell is really awkward. Will need to go to NCL instead
#cd $hist_rundir/
#hist_files=`ls ts_Amon_$model*.nc`
#cp $pp_dir/$climo_file /tmp/$climo_file
#ncrename -v ts,climo /tmp/$climo_file
#for fil in $hist_files
#do
#    cp $fil /tmp/$fil
#    ncks -A -v climo /tmp/$climo_file /tmp/$fil
#    ncap2 -s 'ts=ts-climo' /tmp/$fil $pp_dir/$climo_file $pp_dir/anom$fil
#done

#Remove climatology from hist files
cd $hist_rundir
hist_files=`ls ts_Amon_$model*.nc`
cd $pp_dir
export CLIMO_FILE="$pp_dir/ts_Amon_$model.climo_1961-1990.nc"
export HADCRUT4_FILE="/home/ucfaccb/DATA/obs/HadCRUT.4.5.0.0.median.185001-201612.nc"
for fil in $hist_files
do
    export INPUT_FILE="$hist_rundir/$fil"
    export OUTPUT_FILE="5x5.$fil"
    if [ ! -f $OUTPUT_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
	if [ -f $OUTPUT_FILE ]; then
	    rm $OUTPUT_FILE
	fi
	ncl -n $script_dir/remove_climo_regrid.ncl
	ncks -O --mk_rec_dmn time $OUTPUT_FILE $OUTPUT_FILE
    fi
done

#Remove climatology from rcp85 files
rcp_dir="/data/CMIP/cmip5/output1/BCC/bcc-csm1-1/rcp85/mon/atmos/Amon/r1i1p1/v20120705/ts/"
cd $rcp_dir
rcp_files=`ls ts_Amon_$model*.nc`
cd $pp_dir
export CLIMO_FILE="ts_Amon_$model.climo_1961-1990.nc"
export HADCRUT4_FILE="/home/ucfaccb/DATA/obs/HadCRUT.4.5.0.0.median.185001-201612.nc"
for fil in $rcp_files
do
    export INPUT_FILE="$rcp_dir/$fil"
    export OUTPUT_FILE="5x5.$fil"
    if [ ! -f $OUTPUT_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
	if [ -f $OUTPUT_FILE ]; then
	    rm $OUTPUT_FILE
	fi
	ncl -n $script_dir/remove_climo_regrid.ncl
	ncks -O --mk_rec_dmn time $OUTPUT_FILE $OUTPUT_FILE
    fi
done

#Concat...
hist_str="_historical"
rcp_str="_rcp85"
ncrcat -O 5x5.ts_Amon_$model$hist_str*.nc 5x5.ts_Amon_$model$rcp_str*.nc 5x5.ts_Amon_$model_*.nc 5x5.ts_Anom_$model.rcp85.nc

#Create global mean
ncwa -O -a lat,lon -w wgt -v ts 5x5.ts_Anom_$model.rcp85.nc gm.ts_Anom_$model.rcp85.nc
