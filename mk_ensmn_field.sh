#!/bin/bash

#select ensemble members
ens_mems=`ls /home/ucfaccb/DATA/cmip5_processed_files/*/5x5.ts_Amon_*.rcp??.nc`
mkdir -p /home/ucfaccb/DATA/cmip5_processed_files/ensemble_mean
ncea -O -d time,'1950-01-01','2079-12-31' $ens_mems /home/ucfaccb/DATA/cmip5_processed_files/ensemble_mean/5x5.ts_Anom.ensemble_mean.nc

