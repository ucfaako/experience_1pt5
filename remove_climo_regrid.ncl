; Calculates a variety of oceanic indices, as well as hovmollers, spectra, 
; monthly standard deviations, running standard deviations, and spatial
; composites based on the nino3.4 index. 
;
; Variables used: ts, psl, and tas
;

HADCRUT4_FILE = getenv("HADCRUT4_FILE")  
INPUT_FILE = getenv("INPUT_FILE")  
OUTPUT_FILE = getenv("OUTPUT_FILE")  
CLIMO_FILE = getenv("CLIMO_FILE")

;load HadCRUT4
obsfil=addfile(HADCRUT4_FILE,"r")
lat=obsfil->latitude
lon=obsfil->longitude

;create weights
lat!0="lat"
wgt  = NormCosWgtGlobe(lat)

;load CLIMO_FILE
cfil=addfile(CLIMO_FILE,"r")
climo=cfil->ts
if max(climo&lon).gt.180 then
  climo=lonFlip(climo)
end if

;load INPUT_FILE
ifil=addfile(INPUT_FILE,"r")
ints=ifil->ts
time=ifil->time
;make sure that the dataset goes from -180-180 like HadCRUT4
if max(ints&lon).gt.180 then
  ints=lonFlip(ints)
end if

;create OUTPUT_FILE
ofil=addfile(OUTPUT_FILE,"c")

;Remove climatology on model grid
anoms=calcMonAnomTLL(ints,climo)

;Create new array
ts=new((/dimsizes(time),dimsizes(lat),dimsizes(lon)/),typeof(ints))
ts!0="time"
ts!1="lat"
ts!2="lon"
ts&time=time
ts&lat=lat
ts&lon=lon
copy_VarAtts(ints,ts)

;Bilinear interpolation
ts=linint2(ints&lon,ints&lat,anoms,True,lon,lat,0)
ts@regrid="anomalies regridded from native grid using bilinear interpolation"
ts@climatology="Anomalies wrt climatology of 1961-1990"



;write out to file
ofil->ts = ts
ofil->wgt = wgt

